<?php
/** Enable W3 Total Cache */
define('WP_CACHE', true); // Added by W3 Total Cache

/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, WordPress Language, and ABSPATH. You can find more information
 * by visiting {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'teengamersnetwork');

/** MySQL database username */
define('DB_USER', 'teengame_tgn');

/** MySQL database password */
define('DB_PASSWORD', 'Z0NNWBTpebyV');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '*!I ,HQ{+u4OmSw_B;bb#6AZ|4PO$-6_YHD59FvA}+g<*v*`NW(<Ltdfx5{_x}(-');
define('SECURE_AUTH_KEY',  'G_P-j8F?9!:`&~L(:KWOsp|!L*i{]x:5V B@F~_9i5fwSJUt>!`t5?ZXK&P6`X-j');
define('LOGGED_IN_KEY',    'hwm=1Rvr:R;CtB;$y@XY~mXt,t743VUgFs<Cw21lS1[+Vc(U{/%/1+k8*`8TFPyl');
define('NONCE_KEY',        '!cFI</@:)bo3bMq8N!|nH|X;M ?wDp5-5i7EXl-AKMKL%ttf:G~[(}p{^o9$*@`(');
define('AUTH_SALT',        'p3sBwn*csPx]c|es,W~5+Z6|Q$-JQ);TV9{9?UVk GLnI`71d0?2`4S-Cm`ev3-w');
define('SECURE_AUTH_SALT', 'Bl$JwJ_8RT;E+ODz;Nytqo/ia^NXn>rTt[,1z:~t/D!JexiCo[dxC>`HuD-,l30E');
define('LOGGED_IN_SALT',   '{j_]*l]opa5c`gz+$v,cbzwW9{,y{;>,F>qD@)E--=m} Y=[w^~VBo/QuMr+i4<5');
define('NONCE_SALT',       'fCt6t[s1uqLE6aTDOZf?G,;m1i8)-hw-7k+_y-mwl))/CC{iC(*c+`iHv,$)1]VJ');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * WordPress Localized Language, defaults to English.
 *
 * Change this to localize WordPress. A corresponding MO file for the chosen
 * language must be installed to wp-content/languages. For example, install
 * de_DE.mo to wp-content/languages and set WPLANG to 'de_DE' to enable German
 * language support.
 */
define('WPLANG', '');

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
