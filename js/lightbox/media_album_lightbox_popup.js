function common_popup(popID, popURL){	
    var query= popURL.split('?');
    var dim= query[1].split('&');
    var popWidth = dim[0].split('=')[1]; //Gets the first query string value
    //Fade in the Popup and add close button
    $('#' + popID).fadeIn().css({ 'max-width' : parseInt(popWidth-100)}).prepend('<a onclick="close_popup();" class="close"><img src="/js/lightbox/x.png" class="btn_close" title="Close Window" alt="Close" /></a>');

    //Define margin for center alignment (vertical   horizontal) - we add 80px to the height/width to accomodate for the padding  and border width defined in the css
    var popMargTop = 250;


    var popupwidth=parseInt(popWidth-100) /2;
//    var popMargLeft=((screen.width)/2);
//    var popMargLeft=popupwidth;
    var popMargLeft = ($('#' + popID).width() + 80) / 2;

    //Apply Margin to Popup
    $('#' + popID).css({
        'margin-top' : -popMargTop,
        'margin-left' : -popMargLeft
    });

    //Fade in Background
    $('body').append('<div id="fade"></div>'); //Add the fade layer to bottom of the body tag.
    $('#fade').css({
        'filter' : 'alpha(opacity=80)'
    }).fadeIn(); //Fade in the fade layer - .css({'filter' : 'alpha(opacity=80)'}) is used to fix the IE Bug on fading transparencies 

    return false;	
}



function close_popup(){
    $('#fade , .popup_block').fadeOut(function() {
        jwplayer().stop();
        $('#fade, a.close').remove();  //fade them both out
    });		

}