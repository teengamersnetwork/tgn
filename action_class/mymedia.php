<?php 
include 'query.php';

class mymedia{
    private $hold_data=array();
    private $form_data;
    private $data;
    function mymedia(){
        if(isset ($_GET['removealbum'])){
            $this->remove_album();
        }
        else if(isset ($_GET['makecoverpic'])){
            $this->cover_pic();
        }
        else if(isset ($_GET['friend_media_light_box'])){
            $this->show_friend_media();
        }
        else if(isset ($_GET['media_video_light_box'])){
            $this->show_friend_media_video();
        }
        else if(isset ($_GET['media_video_delete'])){
            $this->media_video_delete();
        }
        else if(isset ($_GET['showvideotags'])){
            $this->showvideotags();
        }
        else if(isset ($_GET['savevideotags'])){
            $this->savevideotags();
        }
        else if(isset ($_GET['deletevideotags'])){
            $this->deletevideotags();
        }
        else if(isset ($_GET['savevideotitle'])){
            $this->savevideotitle();
        }
        else if(isset ($_GET['videocover'])){
            $this->videocover();
        }
        else if(isset ($_GET['videomove'])){
            $this->videomove();
        }
    }
    
    function remove_album(){
      global $query;
      unset ($_GET['removealbum']);
      $this->form_data=$_GET;
      $this->hold_data['succ']=$query->delete('albums',$this->form_data);
      $this->data=$this->form_data['id'];
      $this->hold_data['fileurl']=  $query->get_results("select fileURL from media where albumID='$this->data'");
      if($this->hold_data['fileurl']){
          foreach($this->hold_data['fileurl'] as $fileurl){
              unlink($fileurl->fileURL);
          }
      }
      $this->hold_data['succ']=$query->delete('media',array('albumID'=>$this->data));
      if($this->hold_data['succ']){
            $retarray=array('msgid'=>1,'succ'=>'Album deleted successfully.');
            echo json_encode($retarray);
        }else {
            $retarray=array('msgid'=>2,'error'=>'Error occurred. Please try again later.');
            echo json_encode($retarray);
        }
    }
    
    function cover_pic(){
        global $query;
        $this->form_data['id']=$_GET['picid'];
        $this->form_data['albumid']=$_GET['albumid'];
        
        $query->update('media',array('albumID'=>$this->form_data['albumid']),array('coverpic'=>0));
        $this->data=$query->update('media',array('id'=>$this->form_data['id']),array('coverpic'=>1));
        
        if($this->data){
            $this->data=array('retid'=>1,'succ'=>'Successfully change to cover image.');
            echo json_encode($this->data);
        }else {
            $this->data=array('retid'=>2,'error'=>'Error occurred. Please try again later.');
            echo json_encode($this->data);
        }
    }
    
    function show_friend_media(){
        global $query;
        $this->form_data['albumid']=trim($_GET['albumid']);
        $this->form_data['friendid']=trim($_GET['friendid']);
        $this->form_data['loggeduserid']=trim($_GET['loggeduserid']);
        $this->form_data['albumname']=trim($_GET['albumname']);
        $this->hold_data['albumdata']=$query->get_results("select * from media where albumID=".$this->form_data['albumid']." ");
        if($this->hold_data['albumdata']){
            foreach ($this->hold_data['albumdata'] as $media_info) {
                ?>
                <a href="<?php echo $media_info->fileURL;?>"  title="<?php echo $this->form_data['albumname']; ?>" >
                     <img  id="media_img_<?php echo $media_info->id; ?>"  style="" src="<?php echo $media_info->fileURL;?>" <?php if($media_info->imgtitle!='') echo 'alt="'.$media_info->imgtitle.'"'; ?> />
                </a>
                <?php
            }
        }else echo "Sorry No data found.";
    }
    
    function show_friend_media_video(){
        global $query;
        $this->form_data['albumid']=trim($_GET['albumid']);
        $this->form_data['albumname']=trim($_GET['albumname']);
        $this->form_data['loggedid']=trim($_GET['loggedid']);
        $this->form_data['cur_userid']=trim($_GET['cur_userid']);
        $this->hold_data['albumdata']=$query->get_results("select * from media where albumID=".$this->form_data['albumid']." ");
        if($this->hold_data['albumdata']){
            foreach ($this->hold_data['albumdata'] as $media_video_info) {
                if($media_video_info->videothumpic!='') {
                        $thumbnail=$media_video_info->videothumpic;
                    }else $thumbnail="/images/VLC-Media-Player.jpg";
                ?>
                    
                <div style="margin-right: 15px;width: 65px;float: left;position: relative;">    

                <a id="video_<?php echo $media_video_info->id; ?>"  style="margin-right: 10px;" onClick="popup_video_player('<?php echo $media_video_info->fileURL;?>','<?php echo $thumbnail; ?>','<?php echo $media_video_info->id; ?>');" >
                     <img width="50px" height="50px" style="border: 1px solid gainsboro;padding: 3px;" src="<?php echo $thumbnail;  ?>" <?php if($media_video_info->imgtitle!='') echo 'alt="'.$media_video_info->imgtitle.'"'; ?>  />
                </a>
                <?php    
                if($this->form_data['loggedid']==$this->form_data['cur_userid'] || $this->form_data['loggedid']==1){
                ?>

                <span class="video_<?php echo $media_video_info->id; ?>" style="position: absolute;" onClick="delvideo('<?php echo $media_video_info->id; ?>','<?php echo $media_video_info->fileURL; ?>','<?php echo $media_video_info->videothumpic; ?>')">
                      <img height="19px" width="19px" style="margin:-6px 0 0 -24px;position: absolute;" src="/wp-content/themes/tgn/members/single/media/images/delete.png" />
                </span>
                <?php } ?>
                </div>
                <?php
            }
        }else echo "Sorry No data found.";
    }
    
    function media_video_delete(){
        global $query;
        $this->form_data['mediaid']=trim($_GET['mediaid']);
        $this->form_data['videourl']=trim($_GET['videourl']);
        $this->form_data['previewimgurl']=trim($_GET['previewimgurl']);
        $this->hold_data=$query->delete('media',array('id'=>$this->form_data['mediaid']));
        if($this->hold_data){
            unlink($this->form_data['videourl']);
            unlink($this->form_data['previewimgurl']);
            echo 1;
        }else echo 0;
    }
    
    function showvideotags(){
        global $query;
        $data=$query->getArrayDataOneRow('media',array('id'=>$_GET['videoid']),array('fileTags'));
        if($data['fileTags']!=''){
            $tagarray=explode(',',$data['fileTags']);
        }else{
            $tagarray='';
        }
        
        $titledata=$query->getArrayDataOneRow('media',array('id'=>$_GET['videoid']),array('imgtitle'));
        if($titledata['imgtitle']!=''){
            $title=$titledata['imgtitle'];
        }else{
            $title='';
        }
        
        echo json_encode(array('tag'=>$tagarray,'videoid'=>$_GET['videoid'],'title'=>$title));
        
    }
    
    function savevideotags(){
        global $query;
        if(trim($_GET['tagword'])!=''){
        $exists=$query->getArrayDataOneRow('media',array('id'=>$_GET['videoid']),array('fileTags'));
        if($exists['fileTags']!=''){
            $tagdata=$_GET['tagword'].','.$exists['fileTags'];
        }else $tagdata=$_GET['tagword'];
        $query->update('media',array('id'=>$_GET['videoid']),array('fileTags'=>$tagdata));
        $data=$query->getArrayDataOneRow('media',array('id'=>$_GET['videoid']),array('fileTags'));
        if($data['fileTags']!=''){
            $tagarray=explode(',',$data['fileTags']);
        }
        echo json_encode(array('tag'=>$tagarray,'videoid'=>$_GET['videoid']));
        }
        else return false;
    }
    
    function deletevideotags(){
        global $query;
        $tagword=trim($_GET['tagword']);
        $existsdata=$query->getArrayDataOneRow('media',array('id'=>$_GET['videoid']),array('fileTags'));
        $dataexp=explode(',',$existsdata['fileTags']);
        for($i=0;$i<count($dataexp);$i++){
            if($dataexp[$i]==$tagword){
                unset ($dataexp[$i]);
            }
        }
        if(count($dataexp)>1){
            $updatetag=implode(',', $dataexp);
        }else $updatetag=$dataexp;
        $ret=$query->update('media',array('id'=>$_GET['videoid']),array('fileTags'=>$updatetag));
        if($ret)echo 1;
        else echo 0;
    }
    
    function savevideotitle(){
        global $query;
        $ret=$query->update("media",array('id'=>$_GET['videoid']),array('imgtitle'=>$_GET['videotitle']));
        if($ret){
            echo 1;
        }else echo 0;
    }
    
    function videocover(){
        global $query;
        $albumid=$query->getArrayDataOneRow('media',array('id'=>$_GET['videoid']),array('albumID'));
        $query->update("media",array('albumID'=>$albumid['albumID']),array('coverpic'=>'0'));
        $ret=$query->update("media",array('id'=>$_GET['videoid']),array('coverpic'=>'1'));
        if($ret)echo 1;
        else echo 0;
    }
    
    function videomove(){
        global $query;
        echo $result=$query->rowcount("select id from albums where userID=".$_SESSION['userid']."");
    }
}
$mymedia=new mymedia();
?>