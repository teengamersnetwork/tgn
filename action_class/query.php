<?php
include 'config.php';
class query{
    
    public function insert($tablename,$dataarray){
        $sql="insert into $tablename set ";
        $datas='';
        $i=0;
        foreach ($dataarray as $key=>$value) {
            if($i > 0){
                $datas .=',';
            }
            $datas .= $key.'='."'".mysql_real_escape_string($value)."'";
            $i++;
        }
        $sql .=$datas;
        $res=mysql_query($sql);
        $id=mysql_insert_id();
        if($res):
            return $id;
        else :
            return false;
        endif;
    }
    
    public function update($tablename,$conditionarray,$updatearray){
        $sql="update $tablename set ";
        $data='';
        $i=0;
        foreach ($updatearray as $key=>$dataval) {
            if($i>0){
                $data .=",";
            }
            $data .=$key .'='."'".mysql_real_escape_string($dataval)."'";
            $i++;
        }
        $sql .=$data;
        $sql .=" where ";
        $condata="";
        $j=0;
        foreach ($conditionarray as $conkey=>$conval) {
            if($j>0){
                $condata .=" and ";
            }
            $condata .=$conkey .'='."'".mysql_real_escape_string($conval)."'";
            $j++;
        }
        $sql .=$condata;
        $res=mysql_query($sql);
        if($res){
            return true;
        }
        else {
            return false;
        }
    }
    
    function get_results($sql){
        $SQL=mysql_query($sql);
        $result='';
        if($SQL){
            while ($row = mysql_fetch_object($SQL)){
                $result[]=$row;
            }
          return $result;
        }else {
            return false;
        }
    }
    
    function delete($tablename,$conditionarray){
        $sql="delete $tablename from $tablename where ";
        if($conditionarray){
            $con='';
            $i=0;
            foreach ($conditionarray as $key=>$con_val) {
                if($i>0)$con .=" and ";
                $con .= $key .'='."'".mysql_real_escape_string($con_val)."'";
                $i++;
            }
             $sql .=$con;
            $retval=mysql_query($sql);
            if($retval){
                return TRUE;
            }else return FALSE;
        }
    }
    
   function getArrayDataOneRow($tablename,$conditionarray,$returnarray){
        $field=implode(",", $returnarray);
        $sql="select $field from $tablename where ";
        $where='';
        foreach($conditionarray as $key=>$con_val){
            if($j>0){
                $where .=" and ";
            }
            $where .= $key .'='."'".mysql_real_escape_string($con_val)."'";
            $j++;
        }
        $sql .=$where;
        
        $resarr=$this->get_results($sql);
        $returdata='';
        if($resarr){
            foreach ($resarr as $value) {
                for($t=0;$t<count($returnarray);$t++){
                    $return=$returnarray[$t];
                    $returdata[$return]=$value->$return;
                }
            }
            return $returdata;
        }
        return false;
    }
    
    function rowcount($tablename,$conditionarray,$primaryid) {
        $sql="select $primaryid from $tablename where ";
        $where='';
        foreach($conditionarray as $key=>$con_val){
            if($j>0){
                $where .=" and ";
            }
            $where .= $key .'='."'".mysql_real_escape_string($con_val)."'";
            $j++;
        }
      $sql .=$where;
        
        $sqls = mysql_query($sql);
        if ($sqls) {
            $res = mysql_num_rows($sqls);
            if ($res>0) {
                return $res;
            } else {
                return 0;
            }
        }
    }
   
    
}
$query=new query();
?>
