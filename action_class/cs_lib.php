<?php
/**
 * Description of cs_lib
 *
 * @author tgn
 */
class cs_lib {
    
    function generateRandString($length,$neumericoff=FALSE) {
        $randstr = "";
        for ($i = 0; $i < $length; $i++) {
            
            if($neumericoff==TRUE){
                $randnum = mt_rand(0, 9);
            }else {
                $randnum = mt_rand(0, 61);
            }
            
            if ($randnum < 10) {
                $randstr .= chr($randnum + 48);
            } else if ($randnum < 36) {
                $randstr .= chr($randnum + 55);
            } else {
                $randstr .= chr($randnum + 61);
            }
        }
        return $randstr;
    }
    
    function file_location_url($file_location_name){
        $i = 0;
        while (!file_exists($file_location_name) && $i++ < 10) {
            if($i==10 && !file_exists($file_location_name)){
                $file_location_name="";
            }else {
                  $file_location_name = "../$file_location_name";
            }
        }
        return $file_location_name;
    }
    
    function get_mime_type($file)
{

	// our list of mime types
	$mime_types = array(
		"pdf"=>"application/pdf"
		,"exe"=>"application/octet-stream"
		,"zip"=>"application/zip"
		,"docx"=>"application/msword"
		,"doc"=>"application/msword"
		,"xls"=>"application/vnd.ms-excel"
		,"ppt"=>"application/vnd.ms-powerpoint"
		,"gif"=>"image/gif"
		,"png"=>"image/png"
		,"jpeg"=>"image/jpg"
		,"jpg"=>"image/jpg"
		,"mp3"=>"audio/mpeg"
		,"wav"=>"audio/x-wav"
		,"mpeg"=>"video/mpeg"
		,"mpg"=>"video/mpeg"
		,"mpe"=>"video/mpeg"
		,"mov"=>"video/quicktime"
		,"avi"=>"video/x-msvideo"
		,"flv"=>"video/x-flv"
		,"3gp"=>"video/3gpp"
		,"css"=>"text/css"
		,"jsc"=>"application/javascript"
		,"js"=>"application/javascript"
		,"php"=>"text/html"
		,"htm"=>"text/html"
		,"html"=>"text/html"
	);

	$extension = strtolower(end(explode('.',$file)));

	return $mime_types[$extension];
}
    
}
$cs_lib=new cs_lib();
?>
