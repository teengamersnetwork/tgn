<?php
/** 
 * The base configurations of bbPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys and bbPress Language. You can get the MySQL settings from your
 * web host.
 *
 * This file is used by the installer during installation.
 *
 * @package bbPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for bbPress */
define( 'BBDB_NAME', 'teengame_tgn' );

/** MySQL database username */
define( 'BBDB_USER', 'teengame_tgn' );

/** MySQL database password */
define( 'BBDB_PASSWORD', 'Z0NNWBTpebyV' );

/** MySQL hostname */
define( 'BBDB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'BBDB_CHARSET', 'utf8' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'BBDB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/bbpress/ WordPress.org secret-key service}
 *
 * @since 1.0
 */
define( 'BB_AUTH_KEY', '*!I ,HQ{+u4OmSw_B;bb#6AZ|4PO$-6_YHD59FvA}+g<*v*`NW(<Ltdfx5{_x}(-' );
define( 'BB_SECURE_AUTH_KEY', 'G_P-j8F?9!:`&~L(:KWOsp|!L*i{]x:5V B@F~_9i5fwSJUt>!`t5?ZXK&P6`X-j' );
define( 'BB_LOGGED_IN_KEY', 'hwm=1Rvr:R;CtB;$y@XY~mXt,t743VUgFs<Cw21lS1[+Vc(U{/%/1+k8*`8TFPyl' );
define( 'BB_NONCE_KEY', '!cFI</@:)bo3bMq8N!|nH|X;M ?wDp5-5i7EXl-AKMKL%ttf:G~[(}p{^o9$*@`(' );
/**#@-*/

/**
 * bbPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$bb_table_prefix = 'wp_bb_';

/**
 * bbPress Localized Language, defaults to English.
 *
 * Change this to localize bbPress. A corresponding MO file for the chosen
 * language must be installed to a directory called "my-languages" in the root
 * directory of bbPress. For example, install de.mo to "my-languages" and set
 * BB_LANG to 'de' to enable German language support.
 */
define( 'BB_LANG', 'en_US' );
$bb->custom_user_table = 'wp_users';
$bb->custom_user_meta_table = 'wp_usermeta';

$bb->uri = 'http://teengamersnetwork.com/wp-content/plugins/buddypress/bp-forums/bbpress/';
$bb->name = 'Teen Gamers Network Forums';

define('BB_AUTH_SALT', 'p3sBwn*csPx]c|es,W~5+Z6|Q$-JQ);TV9{9?UVk GLnI`71d0?2`4S-Cm`ev3-w');
define('BB_LOGGED_IN_SALT', '{j_]*l]opa5c`gz+$v,cbzwW9{,y{;>,F>qD@)E--=m} Y=[w^~VBo/QuMr+i4<5');
define('BB_SECURE_AUTH_SALT', 'Bl$JwJ_8RT;E+ODz;Nytqo/ia^NXn>rTt[,1z:~t/D!JexiCo[dxC>`HuD-,l30E');

define('WP_AUTH_COOKIE_VERSION', 2);

?>