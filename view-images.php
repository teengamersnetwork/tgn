<?php session_start(); ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>image view</title>

<script type="text/javascript" src="js/jquery.js">
</script><script type="text/javascript" src="js/jquery_ui_min.js"></script>
<script type='text/javascript' src='css/jquery.tag.js'></script>
<link media="screen" rel="stylesheet" href="css/jquery.tag.css" type="text/css" />
<link media="screen" rel="stylesheet" href="css/jquery-ui.custom.css" type="text/css" />
<link href="css/facebook.alert.css" rel="stylesheet" type="text/css">
<link href="css/facebox.css" media="screen" rel="stylesheet" type="text/css" />

<script src="st/facebox.js" type="text/javascript"></script>
<script type="text/javascript" src="js/jquery_facebook.alert.js"></script>
<script type="text/javascript" src="js/jquery_alert.js"></script>
<script type="text/javascript" src="js/jquery.bgiframe.min.js"></script>
<script type="text/javascript" src="js/jquery.dimensions.js"></script>
<script type="text/javascript" src="123/jquery.autocomplete.js"></script>
<script type="text/javascript" src="js/jquery.autocompletefb.js"></script>
<script type="text/javascript" src="js/localdata.js"></script>
<link rel="stylesheet" type="text/css" href="css/jquery.autocompletefb.css" />

<link rel="stylesheet" type="text/css" href="css/message.css" />
        
<script type="text/javascript" src="js/jquery.pajinate.js"></script>
<link rel="stylesheet" type="text/css" href="css/pagination.css" />


<script>
    
function check_maximum_keyword(id,keywordcount){
    
if(keywordcount!=''){
var $blankno=0;
var $positions=0;
var $com=0;
var $doubleblank=0;
for(var $i=0;$i<keywordcount.length;$i++){

    if($com>10 && keywordcount[$i]!=','){
        $com=0;
        alert('Tagging word not more then 10 character.')
        var $data=keywordcount.substr(0,$positions);
        $("#p"+id).val($data);
    }else {
            var $newpostion=$positions;

            if(keywordcount[$i]==' '){
                $doubleblank++;
                $newpostion=$i;
            }else if(keywordcount[$i]!=' '){
                $doubleblank=0;
            }

            if($doubleblank >= 2){
                alert('Double space not allowed.');
                var $data=keywordcount.substr(0,$newpostion-1);
                $("#p"+id).val($data);
                $newpostion=0;
                $doubleblank=0;
            }
         $com++;
    }

    if(keywordcount[$i]==','){
        $com=0;
        $positions=$i;
        $blankno++;
    } 
    if($blankno==5){
        alert('Maximum 5 tag allow.')
        var $data=keywordcount.substr(0,$positions);
        $("#p"+id).val($data);
    }
  }
 }
}    
    
function delimage(idd)
{		
	jConfirm('Are you sure you want to delete this image?', 'Confirmation Dialog', 
	function(r) {
		if(r==true)
		{
			$("#box").fadeOut(300);	
			$('#img_load').show();
			$.ajax({
				type: "POST",
				url: "save.php?did="+idd,
				success: function(msg){
					//location.reload();
					//return false;
					window.location = "view-images.php?id=<?=$_REQUEST['id']?>&uid=<?php echo $_REQUEST['uid'];?>";
					//window.close();
				}
			});
		}										
	});
}			
function make_cover_pic(idd,albumid)
{		
    var con=confirm('Are you sure you want to make it cover image?');
    if(con){
        $.get('/action_class/mymedia.php?makecoverpic',{picid:idd,albumid:albumid},function(data){
           if(data.retid==1){
           $("#returnmsg").html('<div class="success">'+data.succ+'</div>').show();
           setTimeout('$("#returnmsg").hide()',2000);
           setTimeout('window.reload(true)',2100);

       }else if(data.retid==2){
           $("#returnmsg").html('<div class="error">'+data.error+'</div>').show();
           setTimeout('$("#returnmsg").hide()',2000);
       }
        },'json');
    }
}		

$(document).ready(function(){
    $('#paging_container').pajinate({
            items_per_page :6,
            item_container_id : '.alt_content',
            nav_panel_id : '.alt_page_navigation'

    });
});	


</script>
<style type="text/css">
	
/* Grey Small Dropdown */

/* General dropdown styles */
.dropdown dl{ margin-left:5px; }

.dropdown dd { position:relative; }

/* DT styles for sliding doors */
.dropdown dt a {background:#EEEEEE 
url(privacyOff.png) no-repeat scroll right center;
    display:block; width:40px; height:22px; cursor:pointer;}

.dropdown dt a.selected{
	background:#EEEEEE url(privacyOn.png) 
	no-repeat scroll right center;
}
.dropdown dt a span {
cursor:pointer; display:block; padding:5px;}

/* UL styles */
.dropdown dd ul { 
background:#EEEEEE none repeat scroll 0 0; display:none;
    list-style:none; padding:3px 0; position:absolute;
    left:0px; width:160px; left:auto; right:0;
	border:1px solid #656565; cursor:pointer;z-index:50;
	}
.dropdown dd ul li{ 
background-color:#EEEEEE; margin:0; width:160px;
}
.dropdown span.value { display:none;}
.dropdown dd ul li a { 
display:block; font-weight:normal; width:137px; 
text-align:left; overflow:hidden; padding:2px 4px 3px 19px; 
color:#111111; text-decoration:none;
}
.dropdown dd ul li a:hover{ 
background:#656565; color:white; text-decoration:none; 
}
.dropdown dd ul li a:visited{ 
text-decoration:none; 
}

input.btncrud {
	background-color:#CCCCCC;
	/*margin:5px 0pt 2pt;*/
	border:1px solid;
	/*padding:3px 8px;*/
	font-size:12px;
	color:#000000;
	float:left;
}
.btn2{display:none;}


.imgoverlay {
	position:absolute;
	cursor:pointer;
	z-index:50;
}
</style>


<script language="javascript">
function showpage(e,idd)
{
	//$('#img_load').show();
	
	var theTargetTextArea=$(e.target);
	var bb = theTargetTextArea.val();
	//var bb = $("#"+txtt).val();
	//alert(idd);
	
  	$.ajax({
		type: "POST",
		url: "save.php?idd="+idd+"&txtt="+bb,
		success: function(msg){
			//theTargetTextArea.html(msg);
			//$("#txt").html(msg);
			//$('#img_load').hide();
			//return false;
		}
	});
}

function preventFormSubmit(e)
{
	e.preventDefault();
}
function showpagetag(e,iddd)
{
	
	
	//$('#img_load').show();
	if (e.keyCode == 13) {
        e.preventDefault();
		return false;
    }
	else
	{
		var theTargetTextArea=$(e.target);
		var bbb = theTargetTextArea.val();
		//var bb = $("#"+txtt).val();
//		alert(bbb);
		
		$.ajax({
			type: "POST",
			url: "save1.php?iddd="+iddd+"&txttt="+bbb,
			success: function(msg){
				//theTargetTextArea.html(msg);
				//$("#txt").html(msg);
				//$('#img_load').hide();
				//return false;
			}
		});
	}
}
function tagat(id)
{
	$("#"+id).slideToggle("slow");
}
function moveimage(e,id,txt)
{
	
	var theTargetContainer=$(e.target).closest(".imageManageMenu");
	$('#img_load').show();
	
	//the album list within the currently opened popup
	var theAlbumList=$("#album_"+txt,theTargetContainer);
	var aa = theAlbumList.val();

  	$.ajax({
		type: "POST",
		url: "move.php?id="+id+"&txt="+aa,
		success: function(msg){
			
			//the menu div is removed
			$(".albumImage_"+id+":first").remove();
			
			//the image div is removed
			$(".albumImageMenu_"+id+":first").remove();
			
			//the popup is hidden
			theTargetContainer.closest("#facebox").hide();
			//theTargetContainer.remove();
			<?php /*?>window.location = "view-images.php?id=<?=$_REQUEST['id']?>";<?php */?>
		}
	});
}



</script>
</head>
<body>
<div id="paging_container">
    <div class="alt_page_navigation"></div>
<?php
//Sourav75@
mysql_connect("localhost", "teengame_tgn", "Z0NNWBTpebyV");
mysql_select_db("teengame_tgn");
$uid_ = $_SESSION['USERID'];



$queryal = mysql_query("select * from albums where albumType=1 and userID=".$_REQUEST['uid']);
 $album_no = mysql_num_rows($queryal);
/*
?>
<div style="width:100%"><?php echo $rowal['albumName'];?></div>
<?php*/
$album_id=$_REQUEST['id'];
$query = mysql_query("select * from media where albumID	=".$_REQUEST['id']);
$noall = mysql_num_rows($query);
if($noall>0){
while($row = mysql_fetch_array($query)){
?>
<div id="mydiv"  style="display:none" class="imageManageMenu  albumImageMenu_<?php echo $row['id'];?>">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td bgcolor="#6d84b4" height="30" style="color:#FFF;font-size: 14px;font-weight: bold;padding-left:5px;">Move photo to another album?</td>
  </tr>
  <tr>
    <td style="font-size:13px;padding-left:5px;padding-top:10px;">
    	Select another album for this photo:<br /><br />
        
        <select name="<?php echo $row['id'];?>" id="album_<?php echo $row['id'];?>" style="width:360px;">
        <?php 
			$uiid = $_REQUEST['uid'];
			$result = mysql_query("SELECT * FROM albums where userID ='".$_REQUEST['uid']."' and id<>'".$_REQUEST['id']."' and  albumType = 1 ");
			while($row111 = mysql_fetch_array($result)){
		?>
        	<option value="<?php echo $row111['id'];?>"><?php echo $row111['albumName'];?></option>
        <?php } ?>
        </select>  	
    </td>
  </tr>
  <tr>
  	<td align="right" height="20" valign="top">
    	<input style="background-color:#5972a7;color:#FFF;" type="submit" name="submit" value="Move Photo" onClick="moveimage(event,<?php echo $row['id'];?>,<?php echo $row['id'];?>)"/>
    </td>
  </tr>
</table>

</div>
    
<div class="alt_content">

<div style="float:left;margin:10px 10px 10px 10px;width:250px;" class="albumImage_<?php echo $row['id'];?>">
<table width="25%" border="0" cellspacing="4" cellpadding="4" align="center" style="border:solid 1px #dcdcdc;">
  <tr>
    <td width="48%" rowspan="5" align="right" valign="middle"> 
        <?php 
        $loggeduserid=$_REQUEST['loggeduserid'];
        if($loggeduserid==$uiid || $loggeduserid==1){
        ?>
         <div style="width:162px;height:25px;" align="right">
            <dl style="" class="dropdown">
              <dt>
               <a class="" id="link<?php echo $row['id'];?>" style="cursor: pointer;"></a></dt>
                   <dd>
                    <ul style="display: none;" id="ul<?php echo $row['id'];?>">
                        <!--<li><a id="edit" href="#">Edit Title</a></li>-->
                        <?php if($album_no>1){ ?>
                        <li><a href="#mydiv" rel="facebox">Move to other album</a></li>
                        <?php } ?>
                        <li><a id="delete_confirm" onClick="delimage(<?php echo $row['id'];?>)" href="#">Remove this photo</a></li>
                        <li><a id="makecoverpic" onClick="make_cover_pic(<?php echo $row['id'];?>,<?php echo $album_id; ?>)" href="#">Make Cover Picture</a></li>
                  </ul>
               </dd>
            </dl>
        </div>   
        <?php } ?>
    	<div align="center">
        	<img width="214px" height="137px" src="<?php echo $row['fileURL'];?>" />
            <?php /*?> class="albumImages" id="<?php echo $row['id'];?>"<?php */?>
        </div>
        <hr style="border:solid 1px #dcdcdc" />
        <?php 
		if($row['imgtitle']=='')
		{
			$fnm = "Say something about this photo.";
		}else{
			$fnm = $row['imgtitle'];
		}
                
                
          if($loggeduserid==$uiid || $loggeduserid==1){      
              $readonly='';
          }else $readonly='readonly="readonly"';
	  ?>
        
       	<div id="txt" align="center"> 	
            <textarea onkeydown="check_maximum_keyword('<?php echo $row['id']; ?>',this.value)" <?php echo $readonly; if ($row['imgtitle']=='' && ($loggeduserid==$uiid || $loggeduserid==1)) { ?>  onBlur="if (value== '') {value='Say something about this photo.'}" onFocus="if (value == 'Say something about this photo.') {value=''}" value="Say something about this photo." <?php } ?> onkeyup="showpage(event,<?php echo $row['id'];?>)" id="<?php echo "p".$row['id'];?>" name="<?php echo "p".$row['id'];?>" style="border: 0px solid;resize: none;width:215px;"   ><?php echo $fnm;?></textarea>
            &nbsp;
            <div id="img_load" style="display:none;float:left;">
            	<img src="images/thickbox.gif" />&nbsp;
            </div>&nbsp;      
        </div>  
        <br />
        <div id="<?php echo $row['id']?>" style="display:none;">
        <form autocomplete="on">
        <div align="left">
            <ul class="first acfb-holder" id="autoCompl_<?php echo $row['id']?>">
            	<?php
					
					$query1 = mysql_query("select fileTags from media where id=".$row['id']);
					$row1 = mysql_fetch_array($query1);
					$ft = explode(",",$row1['fileTags']);
					if($row1['fileTags'] !=''){
						foreach($ft as $f){
				?>
                	<div style="width:175px;" id="ac_<?php echo $ft?>"><li class="acfb-data"><span><?php echo $f;?></span> <img class="p" src="delete.gif"/></li></div>
                <?php 
						} 
					} else {
						$query11 = mysql_query("select filetag1 from media where id=".$row['id']);
						$row11 = mysql_fetch_array($query11);
						if($row11['filetag1']!=''){
						?>
                      <div style="width:175px;" id="ac_<?php echo $row11['filetag1'];?>"><li class="acfb-data"><span><?php echo $row11['filetag1'];;?></span> <img class="p" src="delete.gif"/></li></div>  
                <?php
						} }
				?>
                
                <input id="atc_<?php echo $row['id'];?>" type="text" class="city acfb-input" style="width:175px;border:solid 1px #dcdcdc;background-color:#FFFFFF;margin-bottom:0px;color:#000;" onkeydown="showpagetag(event,<?php echo $row['id'];?>)"  />
            </ul>
            <div id="a_<?php echo $row['id'];?>" style="float:left;background: gold;"></div>
        </div>
        </form>
		</div>
		<!--<input type="button" value="Show Value1"  id="show_value1"  class="btncrud" />-->
        
     <br />
     <div style="background-color:#f2f2f2;width:99.5%;height:27px;border:solid 1px #dcdcdc;">
      <img onclick="tagat(<?php echo $row['id']?>);" style="cursor:pointer;float:left;" title="Tag Image" src="tag.jpg" width="28" height="27" alt="tag" />
      <div style="float:left;height:27px;width:1px;background-color:#dcdcdc; ">&nbsp;</div>
    </div>        
    </td>
    
  </tr>
 </table>
    
</div>
</div>
<?php }
} else { ?>
	<div align="center" style="width:100%;height:200px;padding-top:200px;"><img align="absmiddle" src="nophoto.png" width="32" height="32" />There are no photos in this album.<br />
    
    </div>
<?php }
 ?>
</div>
</body>
<script>
	$(document).ready(function(){
		
		$("#tagat").click(function(){
			$("#at").slideToggle("slow");
			//alert('DF');
		});
		
		$(".albumImages").each(function(){
			var theTargetImage=$(this);
			var theImageId=theTargetImage.attr('id');
			
			
			$.getJSON("imageTagAjax.php",{'action':'list','id':theImageId},function(tags){
				
				theTargetImage.tag({
				defaultTags:tags,
			save: function(width,height,top_pos,left,label,the_tag){
				
				$.post("imageTagAjax.php",{'action':'save','width':width,'height':height,'top':top_pos,'left':left,'label':label,'imgid':theImageId},function(id){
					//the_tag.setId(id);
				});
			},
			remove: function(id){
				$.post("imageTagAjax.php",{'action':'delete','id':id});
			}
		});
				//$.each(tags, function(key,tag){
//					$("#"+theImageId).addTag(tag.width,tag.height,tag.top,tag.left,tag.label,tag.id);
//				});
			});
		});
		
		/*$.getJSON("imageTagAjax.php",{'action':'list','id':<?php echo $_REQUEST['id'];?>},function(tags){
			$.each(tags, function(key,tag){
				$("#img").addTag(tag.width,tag.height,tag.top,tag.left,tag.label,tag.id);
			});
		});*/
		
		$('a[rel*=facebox]').facebox();
		
		//var acfb =$("ul.first").autoCompletefb({urlLookup:cities});
		
		$("ul.first").each(function(){
			var thisUl=$(this);
			
			var thisUlId=thisUl.attr('id');
			
			var theImageId=thisUlId.split("autoCompl_")[1];
			
			var acfb=thisUl.autoCompletefb({urlLookup:cities,
			
			onSelection:function(e,d,f){
				$.ajax({
				type: "POST",
				url: "tagsave.php?id="+theImageId+"&txt="+acfb.getData(),
				success: function(msg){
					//$("#ac").html(msg);
					$('#img_load').hide();
				}
			});
			},
			onRemoval:function(o){
				
				$.ajax({
				type: "POST",
				url: "tagsave.php?id="+theImageId+"&txt="+acfb.getData(),
				success: function(msg){
					//$("#ac").html(msg);
					$('#img_load').hide();
				}
			});	
			}
			});
			//alert(acfb.getData());
			
		});
		//$("#show_value1" ).click(function(){
//			$('#img_load').show();
//			//$("#ac").html('');
//			$.ajax({
//				type: "POST",
//				url: "tagsave.php?id="+<?php echo $_REQUEST['id'];?>+"&txt="+acfb.getData(),
//				success: function(msg){
//					//$("#ac").html(msg);
//					$('#img_load').hide();
//				}
//			});
//		});
		//$("#clear_value1").click(function(){acfb.clearData();	  });
		
		//function acfbuild(cls,url){
//			var ix = $("input"+cls);
//			ix.addClass('acfb-input').wrap('<ul class="'+cls.replace(/\./,'')+' acfb-holder"></ul>');
//			
//			return $("ul"+cls).autoCompletefb({urlLookup:url});
//		}
				
		$("#edit").click(function () {
			$('#txt').toggle();
			$('#lbl').toggle();
			//$('#edit').hide();
		});
		
		$(".dropdown dt a").click(function() {

		        // Change the behaviour of onclick states for links within the menu.
			var toggleId = "#" + this.id.replace(/^link/,"ul");

		        // Hides all other menus depending on JQuery id assigned to them
			$(".dropdown dd ul").not(toggleId).hide();

		        //Only toggles the menu we want since the menu could be showing and we want to hide it.
			$(toggleId).toggle();

		        //Change the css class on the menu header to show the selected class.
			if($(toggleId).css("display") == "none"){
				$(this).removeClass("selected");
			}else{
				$(this).addClass("selected");
			}

		});

		$(".dropdown dd ul li a").click(function() {

		    // This is the default behaviour for all links within the menus
		    var text = $(this).html();
		    $(".dropdown dt a span").html(text);
		    $(".dropdown dd ul").hide();
		});

		$(document).bind('click', function(e) {

		    // Lets hide the menu when the page is clicked anywhere but the menu.
		    var $clicked = $(e.target);
		    if (! $clicked.parents().hasClass("dropdown")){
		        $(".dropdown dd ul").hide();
				$(".dropdown dt a").removeClass("selected");
			}

		});
		
	});
</script>
<div id="returnmsg" class="popup_callback"><!--do not remove this tag--></div>
</html>
