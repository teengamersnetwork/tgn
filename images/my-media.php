<?php get_header()?>

<div id="content">
<div class="padder">
<div id="item-header">
<?php locate_template( array( 'members/single/member-header.php' ), true ) ?>
</div>

<div id="item-nav">
<div class="item-list-tabs no-ajax" id="object-nav">
<ul>
<?php bp_get_displayed_user_nav() ?>
</ul>
</div>
</div>

<div class="item-list-tabs no-ajax" id="subnav" role="navigation">
<ul>
<?php bp_get_options_nav() ?>

</ul>

</div><!-- .item-list-tabs -->            


<div id="item-body">
<?php 
global $bp,$wpdb;
$loggedUserID = $bp->loggedin_user->id;	
$userName = $bp->loggedin_user->fullname;
$domain = $bp->loggedin_user->domain;	
if ( isset( $bp->displayed_user->id ) )
$uID = $bp->displayed_user->id;
elseif ( isset( $bp->loggedin_user->id ) )
$uID= $bp->loggedin_user->id;


if($loggedUserID!='')
{
// CHECK FOR ALBUMS
$video_album_no=$wpdb->get_var( $wpdb->prepare( "SELECT COUNT(*) FROM albums WHERE userID=$uID and albumType=2 " ) );
$images_album_no=$wpdb->get_var( $wpdb->prepare( "SELECT COUNT(*) FROM albums WHERE userID=$uID and albumType=1 " ) );
$album_no=$wpdb->get_var( $wpdb->prepare( "SELECT COUNT(*) FROM albums WHERE userID='$uID' " ) );

if($album_no==0){
?>
    <a href='<?=$domain?>media/create-album/'>Create Album</a>
<?php
    }else {
?>
<script type="text/javascript">
var GB_ROOT_DIR = "http://teengamersnetwork.com/modal/";					
</script>
<script type="text/javascript" src="/modal/AJS.js"></script>
<script type="text/javascript" src="/modal/AJS_fx.js"></script>
<script type="text/javascript" src="/modal/gb_scripts.js"></script>
<link href="/modal/gb_styles.css" rel="stylesheet" type="text/css" />	

<link href="css/style1.css" type="text/css" rel="stylesheet" media="all" />
<link rel="stylesheet" type="text/css" href="/css/message.css" />
<script type="text/javascript" src="/js/jquery-latest.js"></script>


<link rel="stylesheet" type="text/css" href="/lightbox/lightbox.css" />
<script type="text/javascript" src="/lightbox/lightbox.js"></script>
<script type="text/javascript" src="/lightbox/lightbox_popup.js"></script>

<script type="text/javascript" src="/wp-content/theme/tgn/mediaplayer/jwplayer.js"></script>

<script type="text/javascript" src="/js/jquery.pajinate.js"></script>
<link rel="stylesheet" type="text/css" href="/css/pagination.css" />
<script type="text/javascript">	

function delalbum(albumid)
{		
var con=confirm('Are you sure you want ot delete this Album?');
if(con) {
$.get('/action_class/mymedia.php?removealbum',{id:albumid},function(data){
if(data.msgid==1){
$("#returnmsg").html('<div class="success">'+data.succ+'</div>').show();
setTimeout('$("#returnmsg").hide()',2000);
setTimeout('$("#album'+albumid+'").hide("bind")',2100);
var albumno=$("#album_count").text();
$("#album_count").text(parseInt(albumno)-1);
}else if(data.msgid==2){
$("#returnmsg").html('<div class="error">'+data.error+'</div>').show();
setTimeout('$("#returnmsg").hide()',2000);
}
},'json');
}
}

function show_lightbox(albumid,friendid,loggeduserid){
$.get('/action_class/mymedia.php?friend_media_light_box',{albumid:albumid,friendid:friendid,loggeduserid:loggeduserid},function(data){
    $("#loader").show();
    $("#show_images").hide();
    $("#show_images").html(data).slideDown(500);
    $("#loader").hide();
    $('.album_images a').lightBox();
    $('#show_images a').CS_Zoomer({speedView:200,speedRemove:400,altAnim:true,speedTitle:400,debug:false});
});
}


function show_lightbox_video(albumid,albumname,loggedid,cur_userid){
   $.get('/action_class/mymedia.php?media_video_light_box',{albumid:albumid,albumname:albumname,loggedid:loggedid,cur_userid:cur_userid},function(data){
        $("#loader-video").show();
        $("#show_videos").hide();
        $("#show_videos").html(data).slideDown(500);
        $("#loader-video").hide();
    });
}

function popup_video_player(videourl,previewimg){
    jwplayer("container").setup({
    flashplayer:"/wp-content/themes/tgn/mediaplayer/player.swf",
    file : videourl,
    height :350,
    width : 450,
    image:previewimg
    });
var popID = 'show_friend_media_video'; //Get Popup ID
var popURL = '#?w=700'; //Get Popup href to define size	
common_popup(popID, popURL);
}

function delvideo(mediaid,videourl,previewimgurl){
    var con=confirm("Do you want to delete this video ?");
    if(con){
    $.get('/action_class/mymedia.php?media_video_delete',{mediaid:mediaid,videourl:videourl,previewimgurl:previewimgurl},function(data){
        if(data==1){
        $("#returnmsg").html('<div class="success"> Video deleted successfully. </div>').show();
        setTimeout('$("#returnmsg").hide()',2000);
        setTimeout('$("#video_'+mediaid+'").hide("bind")',2100);
        setTimeout('$(".video_'+mediaid+'").hide("bind")',2100);
        }else if(data==0){
        $("#returnmsg").html('<div class="error">Error occurred !! Please try again later.</div>').show();
        setTimeout('$("#returnmsg").hide()',2000);
        }
    });
    }
}
    



 
</script>


<style>
    .ellipse,.first_link,.last_link{
        display: none !important;
    }
    
#show_images {float: left;list-style: none;margin: 0; padding: 10px;width: 800px;background-color: white;}
#show_images a {margin: 0; padding: 5px;float: left;position: relative;width: 60px;height: 60px;text-decoration: none; }
#show_images a img { padding: 5px; background: none repeat scroll 0 0 #F0F0F0;border: 1px solid #DDDDDD; width: 50px; height: 50px;position: absolute;left: 0; top: 0; opacity:.7;}
#show_images img:hover {margin-top:35px;background:url(/images/thumb_bg.png) no-repeat center center;border: none;}
.title{position:absolute;width:160px;overflow: hidden;margin-left:0px;font-weight:900;font-size: 10px;background:url(/images/blue.png) no-repeat center center;padding:20px 0 0 0;text-align:center; color: #fff; }


</style>    

<?php 
$result_array=$wpdb->get_results("select * from albums WHERE userID = '$uID' ");
if($video_album_no!=0) { ?>

<div id="item-albums">
<h3>Video Albums (<span id='album_count'><?php echo $video_album_no;?></span>) </h3>
<?php
    if($result_array){
        foreach($result_array as $video_info){
            if($video_info->albumType==2){
?>

<div class="maindivborder" id="album<?php echo $video_info->id; ?>" style="float:left;padding:5px;border:solid 1px #999;margin:10px;border: 6px ridge #6D84B4;">
    <?php if($loggedUserID==$uID || $loggedUserID==1) { ?>
    <span style="float: right;"><a onClick="delalbum('<?php echo $albumid; ?>')" ><img height="20px" width="20px" style="margin: -15px -5px;position: absolute;" src="/wp-content/themes/tgn/members/single/media/images/delete.png" /></a></span>
    <?php } 
   // if($loggedUserID==$uID){
    ?>

    <?php
//    }
//    else {
    ?>
    <a title="<?php echo $video_info->albumName; ?>" onclick="show_lightbox_video('<?php echo $video_info->id; ?>','<?php echo $video_info->albumName; ?>','<?php echo $loggedUserID; ?>','<?php echo $uID; ?>');" >
    <?php //} ?>    
    <div class="imageContainer">
    <div style="height:105px;width:125px;">
    <img height="125px" width="125px" src="/wp-content/themes/tgn/members/single/media/images/noimage.png" />
    </div>
    <br />
    <span><center><?php echo $video_info->albumName; ?></center></span>
    </div>
    </a>
</div>
<?php
    } } }?>
</div>
<div style="clear:both;">
    <div id="loader-video" style="display: none;"><img src="/images/ajax-loader.gif" height="32"  width="32"/></div>
    <div id="show_videos" class="album_videos" style="clear: both;margin-bottom: 20px;display: none;margin-left: 10px;">&nbsp;</div>
</div>

<?php } ?>

<?php
    if($images_album_no!=0){
            ?>
          <script type="text/javascript" src="/jqlightbox/jquery.js"></script>
        <script type="text/javascript" src="/jqlightbox/jquery.lightbox-0.5.js"></script>
            <link rel="stylesheet" type="text/css" href="/jqlightbox/jquery.lightbox-0.5.css" />
          <script type="text/javascript" src="/js/img_proportional.js"></script>
<!--            <script >
            $(document).ready(function(){
                
            });    
            </script>-->
            <div id="item-albums" style="clear: both;">
            <h3>Picture Albums (<span id='album_count'><?php echo $images_album_no;?></span>)</h3>
            <?php 
            
                foreach($result_array as $images_info){
                if($images_info->albumType==1){

                $query1 = $wpdb->get_results("select fileURL from media where coverpic=1 and albumID=$images_info->id" );
                $fileurl='';
                if($query1){
                    foreach ($query1 as $cover_pic) {
                        $fileurl =$cover_pic->fileURL;  
                    }
                }
            
            ?>
                
                <div class="maindivborder" id="album<?php echo $images_info->id; ?>" style="float:left;padding:5px;border:solid 1px #999;margin:10px;border: 6px ridge #6D84B4;">
                <?php if($loggedUserID==$uID || $loggedUserID==1) {?>
                <span style="float: right;"><a onClick="delalbum('<?php echo $images_info->id; ?>')" ><img height="20px" width="20px" style="margin: -15px -5px;position: absolute;" src="/wp-content/themes/tgn/members/single/media/images/delete.png" /></a></span>
                <?php } 
                if($loggedUserID==$uID){
                ?>
                <a title="<?php $images_info->albumName; ?>" rel="gb_page_center[860, 515]" href="/view-images.php?id=<?php echo $images_info->id; ?>&uid=<?php echo $uID;?>&loggeduserid=<?php echo $loggedUserID;?>">
                <?php
                }
                else {
                ?>
                <a title="<?php echo $images_info->albumName; ?>" onclick="show_lightbox('<?php echo $images_info->id; ?>',false,false,'<?php echo $images_info->albumName; ?>');" >
                <?php } ?>    
                <div class="imageContainer">
                <div style="height:105px;width:125px;">
                <?php
                if($fileurl !=''){
                echo '<img height="110px" width="125px" src="'.$fileurl.'" />';
                }else{
                ?>
                <img height="125px" width="125px" src="/wp-content/themes/tgn/members/single/media/images/noimage.png" />
                <?php } ?>
                </div>
                <br />
                <span><center><?php echo $images_info->albumName; ?></center></span>
                </div>
                </a>
                </div>
            <?php } } ?>
            <div style="clear:both;">
                <div id="loader" style="display: none;"><img src="/images/ajax-loader.gif" height="32"  width="32"/></div>
                <div id="show_images" class="album_images" style="clear: both;margin-bottom: 20px;margin-left: 10px;display: block;">&nbsp;
                
<!--                <a title="" href="http://s3.amazonaws.com/tgn-cdn/1/2.jpg">
<img id="media_img_178" title="eice Taj" src="http://s3.amazonaws.com/tgn-cdn/1/2.jpg" style="" alt="test">
</a>-->
                </div>
            </div>
           
        <?php
          }
        ?>


<!------------------------------------light box------------------------------------------->
<div id="show_friend_media_video" class="popup_block" style="max-width: 600px;" >
<center>
<div style="max-height:460px; width:95%; overflow: hidden;" >
<div id="show_friend_media_video" style="" class="wrap">
<div id="container">Loading the player</div>
</div>
</div>
</center>
</div>
<!------------------------------------light box------------------------------------------->
    
<?php     
    }   
}else echo "Please log in to view media.";
?>
    
</div><!-- #item-body -->
</div><!-- .padder -->
</div><!-- #content -->
<div id="returnmsg" class="popup_callback"><!--do not remove this tag--></div>
<?php locate_template( array( 'sidebar.php' ), true ) ?>

<?php get_footer() ?>

